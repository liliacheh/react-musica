import { Component } from "react";
import { FavIcon, CartIcon} from "../icons"
import style from "./header.module.scss"
import PropTypes from 'prop-types';

export default class Header extends Component{
    render(){
        return(
            <>
            <header className={style.header}>
                    <div className={style.header__container}>
                        <div className={style.header__wrapper}>
                            <a href='/' className={style.header__logo}>
                                <img className={style.header__img} src="./logo.svg" alt="logo" />
                            </a>
                            <div className={style.header__btns}>
                                <a href='/' className={style.header__favorites} title="Favorites">
                                    <FavIcon />
                                    {this.props.favorites > 0 && <div>{this.props.favorites}</div>}
                                </a>
                                <a href='/' className={style.header__cart} title="Cart">
                                    <CartIcon />
                                    {this.props.cart > 0 && <div>{this.props.cart}</div>}
                                </a>
                            </div>
                        </div>
                    </div>
                </header>

            </>
        )
    }
}
Header.propTypes = {
    favorites: PropTypes.number,
    cart: PropTypes.number
}
Header.defaultProps = {
    favorites: 0,
    cart: 0
}

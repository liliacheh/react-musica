import style from "./productList.module.scss";
import { Component } from "react";
import ProductItem from "../productItem";
import PropTypes from 'prop-types';

export default class ProductList extends Component{
   
    render(){
        const { products, addToFavList, favorites, openModal, addToCart } = this.props
        return(
            <div className={style.productList}>
            <div className={style.productList__container}>
                <div className={style.productList__wrapper}>
                    {products.map(product => 
                        <ProductItem
                            key={product.article}
                            product={product}
                            openModalAddToCart={openModal}
                            addToFavList={addToFavList}
                            addToCart={addToCart}
                            favorites={favorites} 
                            />
                            
                    )}
                </div>
            </div>
        </div>

        )
    }

}
ProductList.propTypes = {
    addToFavList: PropTypes.func,
    // addToCart: PropTypes.func,
    favorites: PropTypes.array,
    openModal: PropTypes.func,
    product: PropTypes.array
}

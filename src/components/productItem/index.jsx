import { Component } from "react";
import style from "./productItem.module.scss"
import Button from "../button";
import {FavIcon} from "../icons";
import PropTypes from 'prop-types';
import no_image from "../../img/no_image.png"

export default class ProductItem extends Component {
    constructor(props) {
        super(props) 
        this.state = {
            isFavorite: false,
           
        }
    }
   
componentDidMount = () =>  {
    
    this.props.favorites.includes(this.props.product.article) && this.setState(state => ({ ...state, isFavorite: true }))
    
}
toggleIsFav = ()=> {
    this.state.isFavorite ? this.setState(state => ({ ...state, isFavorite: false })) : this.setState(state => ({ ...state, isFavorite: true }))
}
// isFav = (e)=> {
//     e.preventDefault()
//     this.state.isFavorite ? e.target.setAttribute('fill','white') : e.target.setAttribute('fill','black')
// }

render(){
    const product = this.props.product

    return(
<a href='/' className={style.product}>
                <header className={style.product__header}>
                    <h3 className={style.product__name}>{product.name}</h3>
                    
                </header>
                <div className={style.product__body}>
                    <div className={style.product__img}>
                        <img src={product.imgUrl || no_image} alt={product.name} />
                    </div>
                    <div className={style.product__descr}>
                    <div className={style.product__article}>{product.article}</div>
                        <div className={style.product__color}>{product.color}</div>
                        <div className={style.product__price}>{product.price} <span>грн</span></div>
                        <div className={style.product__btns}>
                            <Button className={style.product__favBtn}
                            onClick={(e)=> { 
                                // this.isFav(e)   
                                this.props.addToFavList(e, product.article)                            
                                this.toggleIsFav()}}
                                title='Add to favorites'
                            ><FavIcon fill={this.state.isFavorite ? "black":"white"}/>
                            </Button>
                            <Button className={style.product__buyBtn}
                            onClick={(e)=> {
                                this.props.openModalAddToCart(e, 'buy', product.article) 
                                
                        }}
                            text={"Add to cart"}>
                            </Button>
                        </div>
                        </div>
                        </div>
    
                        </a >
)
}
    

}
ProductItem.propTypes = {
    addToFavList: PropTypes.func,
    favorites: PropTypes.array,
    openModalAddToCart: PropTypes.func,
    addToCart: PropTypes.func,
    product: PropTypes.shape({
        name: PropTypes.string,
        article: PropTypes.number,
        imgUrl: PropTypes.string,
        color: PropTypes.string,
        price: PropTypes.number,
    })
}

ProductItem.defaultProps = {
    product: {
        imgUrl: no_image,
    },
}

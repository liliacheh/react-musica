import { Component } from "react";
import style from "./modal.module.scss"
import PropTypes from 'prop-types'

export default class Modal extends Component {
 
    componentDidMount() {
        document.addEventListener("mousedown", this.handleClickOutside);
    }

    componentWillUnmount() {
        document.removeEventListener("mousedown", this.handleClickOutside);
    }

    handleClickOutside = (e) => {
        const modal = document.querySelector("#modal__body");
        if (modal && !modal.contains(e.target)) {
            this.props.closeModal();
        }
    };
    render(){
    const modalProps = this.props.data
    const props = this.props

        return(
            <div className={style.modal}>
                <div className={style.modal__body} id="modal__body">
                    <div className={style.modal__header}>
                    <h3 className={style.modal__title}>{modalProps.header}</h3>
                    {props.closeButton && modalProps.closeBtn(style.modal__close, props.closeModal)}
                    </div>
                    <div className={style.modal__text}>
                    {modalProps.text}
                    </div>
                    <div className={style.modal__btns}>
                    {modalProps.actions && modalProps.actions(style.modal__btn,props.addToCart, props.closeModal)}

     </div>
                </div>
            </div>
        )
    }
}
Modal.propTypes = {
    closeButton: PropTypes.bool,
    closeModal: PropTypes.func,
    addToCart: PropTypes.func,
    data: PropTypes.shape({
        id: PropTypes.string,
        header: PropTypes.string,
        text: PropTypes.object,
        closeBtn: PropTypes.func,
        actions: PropTypes.func,    
    })
    


}
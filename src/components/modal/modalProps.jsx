export const modalProps = [
{
    id: 'buy',
    header: "Add product to cart?",
    closeButton: true,
    text: <><p>Are you sure you want to add this item to your cart?</p></>,
    
    closeBtn(className, closeHandler ){
        return(
        <button className={className} onClick={closeHandler}></button>
    )
},
    actions(className, addToCart, cancel){
        return(
        <>
 <button className={className} onClick={addToCart}>Add to cart</button>
 <button className={className} onClick={cancel}>Cancel</button>
        </>
    )
}
}
]
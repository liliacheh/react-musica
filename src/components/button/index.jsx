import { Component } from "react";
import PropTypes from 'prop-types';

export default class Button extends Component {
  
    render() {

        return (
            <button
                className={this.props.className}
                onClick={this.props.onClick}
                type={this.props.type}
                title={this.props.title}
            >
                {this.props.text}
                {this.props.children}
            </button>
        )
    }
}

Button.propTypes = {
    className: PropTypes.string,
    onClick: PropTypes.func,
    type: PropTypes.string,
    title: PropTypes.string,
    text: PropTypes.string,
    children: PropTypes.object
}

Button.defaultProps = {
    type: 'button',
}


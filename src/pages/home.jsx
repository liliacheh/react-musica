import { Component } from "react";
import Modal from '../components/modal';
import { modalProps } from '../components/modal/modalProps';
import ProductList from "../components/productList";
import Header from "../components/header";

export default class Home extends Component{
    constructor(props){
        super(props)
    
        this.state = { 
            isOpen : null,
            products: [],
            favorites: [],
            cart : [],
            article: null,
            error: null
         }
        this.openModal = this.openModal.bind(this)
        this.closeModal = this.closeModal.bind(this)
    }
    componentDidMount(){
        fetch('./products.json')
            .then(res => res.json())
            .then(
                (result) => { this.setState(state => ({ ...state, products: result }))
                try {
                  const favorites = JSON.parse(localStorage.getItem('favorites')) || [];
                  const cart = JSON.parse(localStorage.getItem('cart')) || [];
                  this.setState(state => ({ ...state, favorites, cart }));
                } catch (error) {
                  console.error('Error reading data from localStorage:', error);
                }
              },
                (error) => { this.setState(state => ({ ...state, error }))}
            )
          //   if (localStorage.getItem('favorites')) {
          //     this.setState(state => ({
          //         ...state,
          //         favorites: JSON.parse(localStorage.getItem('favorites')),
          //         cart: JSON.parse(localStorage.getItem('cart'))
          //     }))
          // } else {
          //     localStorage.setItem('favorites', JSON.stringify([]))
          //     localStorage.setItem('cart', JSON.stringify([]))
          // } 
          if (!localStorage.getItem('favorites')) {
            localStorage.setItem('favorites', JSON.stringify([]))
            localStorage.setItem('cart', JSON.stringify([]))
            }
      

    }
    componentDidUpdate(prevProps, prevState) {
      if (prevState.favorites !== this.state.favorites) {
          localStorage.setItem('favorites', JSON.stringify(this.state.favorites))
      } else if (prevState.cart !== this.state.cart) {
          localStorage.setItem('cart', JSON.stringify(this.state.cart))
      }   
  }

    openModal (e, id, article) {
      e.preventDefault()
      this.setState({...this.state, isOpen: id, article})
    };
    
    closeModal(){
      this.setState(state => ({...state, isOpen: null, article: null}))
    }

    // addToFavList = (e, article) => {
    //   e.preventDefault()

    //   const newFavListItem = this.state.products.find(product => product.article === article)
    //   const newFavListItemIndex = this.state.favorites.indexOf(newFavListItem.article)
      
    //   newFavListItemIndex === -1?
    //   this.setState({...this.state, favorites: [...this.state.favorites, newFavListItem.article]}):
    //   this.setState(state => ({ ...state, favorites: [...state.favorites.slice(0, newFavListItemIndex), ...state.favorites.slice(newFavListItemIndex + 1)] }))
    // }
    addToFavList = (e, article) => {
      e.preventDefault()
      if (!this.state.favorites.includes(article)) {
        this.setState((state) => ({ favorites: [...state.favorites, article] })); 
    } else {
      const newFavorites = this.state.favorites.filter(fav => fav !== article)
      this.setState({ favorites: newFavorites})
    }
  }
    
    // addToCart = () =>{
    //   const newCartItem = this.state.products.find(product => product.article === this.state.article)
    //   this.setState(state => ({ ...state, cart: [...state.cart, newCartItem.article] }))
    //   this.closeModal()
    // }
    addToCart = () =>{
      const newCartItem = this.state.products.find(product => product.article === this.state.article);
      const itemInCartIndex = this.state.cart.findIndex(item => item.id === newCartItem.article);
    
      if (itemInCartIndex === -1) {
        // add new item to cart
        const newItem = { id: newCartItem.article, quantity: 1 };
        this.setState(state => ({ ...state, cart: [...state.cart, newItem] }));
      } else {
        // increment item quantity in cart
        const updatedItem = { ...this.state.cart[itemInCartIndex], quantity: this.state.cart[itemInCartIndex].quantity + 1 };
        const updatedCart = [...this.state.cart.slice(0, itemInCartIndex), updatedItem, ...this.state.cart.slice(itemInCartIndex + 1)];
        this.setState(state => ({ ...state, cart: updatedCart }));
      }
    
      this.closeModal();
    }
    

      render (){ 

        return(
       <>
       {this.state.isOpen 
       && <Modal 
       data={modalProps.find(modal => modal.id === this.state.isOpen)}
       closeModal= {this.closeModal}
       addToCart={this.addToCart}
       closeButton={modalProps.find(modal => modal.id === this.state.isOpen).closeButton}/> }

       <Header
       favorites={this.state.favorites.length}
       cart={this.state.cart.length}
/>
{!this.state.error ?
       <ProductList 
       products={this.state.products}
       openModal={this.openModal}
       addToFavList={this.addToFavList}
       favorites={this.state.favorites}
       />:
       <div>There was an error loading the product list, please try reload the page</div>}
       </>
       
        )
    };
}
